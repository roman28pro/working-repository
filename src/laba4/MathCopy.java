package laba4;

public class MathCopy {

    public static double pow(double a, double b) {
        return java.lang.Math.pow(a, b);
    }

    public static double cos(double x) {
        return java.lang.Math.cos(x);
    }

    public static double sin(double x) {
        return java.lang.Math.sin(x);
    }

    public static double tan(double x) {
        return java.lang.Math.tan(x);
    }

    public static double sec(double x) {
        return 1 / cos(x);
    }

    public static double cot(double x) {
        return 1 / tan(x);
    }

    public static double csc(double x) {
        return 1 / sin(x);
    }

    public static double ln(double x) {
        return java.lang.Math.log10(x);
    }

    public static double log(double a, double b) {
        double result = 0;
        for (int i = 1; a >= b; i++) {
            a/=b; result = i++;
        }
        return result;
    }
}
