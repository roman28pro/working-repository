package laba4;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Введите х: ");
        Scanner sc = new Scanner(System.in);
        try {
            System.out.println("Ответ: " + answer(sc.nextDouble()));
        } catch (Exception e) {
            System.out.println("Вернись и введи нормально!");
        }
    }

    public static double answer(double x) {
        if (x <= 0) {
            return (MathCopy.pow((((MathCopy.sec(x) - MathCopy.cot(x)) + MathCopy.cos(x)) * MathCopy.tan(x)) + (MathCopy.csc(x) / MathCopy.csc(x) / MathCopy.cot(x)), 2));
        } else {
            return (((((MathCopy.log(x, 3) - MathCopy.log(x, 3)) - MathCopy.log(x, 5)) * (MathCopy.log(MathCopy.pow(x, 2),10 ))) * (MathCopy.ln(x) * (MathCopy.log(x, 3) / MathCopy.ln(x) + MathCopy.log(x, 10)))) + ((MathCopy.log(MathCopy.pow(x, 2), 10)) + MathCopy.log(x, 10)));
        }
    }
}
