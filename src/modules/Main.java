package modules;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Введите число для подстановки в уравнение ((х+х)*5/2)^2");
        System.out.println(new Module3().calculate(new Module2().calculate(new Module1().calculate(new Scanner(System.in).nextDouble()))));
    }
}

