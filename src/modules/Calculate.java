package modules;

public interface Calculate {

    double calculate(double number);

}
