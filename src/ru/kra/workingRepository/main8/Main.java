package ru.kra.workingRepository.main8;

public class Main extends Thread{

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            (new Main()).start();
        }
        System.out.println("Главный поток запущен");

    }

    @Override
    public void run() {
        System.out.println("Привет из потока под названием " + getName());
    }
}

