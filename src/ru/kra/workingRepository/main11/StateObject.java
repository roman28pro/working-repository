package ru.kra.workingRepository.main11;

/**
 * Счетчик
 *
 */

public class StateObject {
    private int i;

    synchronized void increment(){
        i++;
    }

    public int getI(){
        return i;
    }
}