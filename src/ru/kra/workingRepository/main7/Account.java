package ru.kra.workingRepository.main7;

public class Account {
    private long balance;

    public Account() {
        this(0);
    }

    public Account(long balance) {
        this.balance = balance;
    }

    public long getBalance() {
        return balance;
    }

    public void addMoney(long money) {
        negativeNumСheck(money);
        balance += money;
    }

    public void takeMoney(long money) {
        negativeNumСheck(money);
        balance -= money;
    }

    private void negativeNumСheck(long money) {
        if (money < 0) {
            throw new ArithmeticException("Введённое вами число меньше 0");
        }
    }

    void NegativeBalance() {
        if (getBalance() < 0) {
            System.out.println("Баланс отрицателен. Ошибка");
        }
    }
}