package ru.kra.workingRepository.nio.main10;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

class CopyFile {
    private Path inputFileWay, outputFileWay;

    CopyFile(String inputFileWay, String outputFileWay) {
        this.inputFileWay = Paths.get(inputFileWay);
        this.outputFileWay = Paths.get(outputFileWay);
    }

    void copy() {
        try {
            Files.copy(inputFileWay, outputFileWay, StandardCopyOption.REPLACE_EXISTING);
        } catch (FileNotFoundException e) {
            System.out.println("Файл не найден!");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}