package ru.kra.workingRepository.nio.main10;

public class CopyStream extends Thread {
    private String way, way_2;

    CopyStream(String way, String way_2) {
        this.way = way;
        this.way_2 = way_2;
    }

    @Override
    public void run() {
        CopyFile cf = new CopyFile(way, way_2);
        cf.copy();
    }
}
