package ru.kra.workingRepository.calculator2;

class Parse {

    /**
     * Метод преобразует строку в целочисленное значение
     *
     * @param str - строка
     * @return - преобразованное значение
     */
    static int parseInt(String str) {
        return Integer.parseInt(str);
    }
}