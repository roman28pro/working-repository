package ru.kra.workingRepository.calculator2;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

class ReadFile {

    /**
     * Метод считывает с консоли строку
     *
     * @return - строку
     */
    static String inputConsole() {
        Scanner sc = new Scanner(System.in);

        String expression = sc.nextLine();

        dataCheck(expression);

        return expression;
    }

    /**
     * Метод считывает данные из файла
     *
     * @param url - строковый путь к файлу
     * @return - строковый массив
     */
    static ArrayList<String> inputFile(String url) {
        ArrayList<String> expressions = new ArrayList<String>();

        try (BufferedReader br = new BufferedReader(new FileReader(url))) {

            String str;
            while ((str = br.readLine()) != null) {
                if (dataCheck(str)) {
                    expressions.add(str);

                } else {
                    str = "";
                    expressions.add(str);
                }
            }

        } catch (FileNotFoundException e) {
            System.out.println("Ошибка: Файл не найден!");
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return expressions;
    }

    /**
     * Метод проверят входящие данные
     *
     * @param expression - срока
     */
    private static boolean dataCheck(String expression) {
        return expression.matches("[+-]?[0-9]+\\s[+-/*^%]?\\s[+-]?[0-9]+");
    }
}