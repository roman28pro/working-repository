package ru.kra.workingRepository.calculator2;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

class WriteFile {
    /**
     * Метод выыводит строку в консоль
     *
     * @param answer - строка
     */
    static void outputConsole(String answer) {
        System.out.println("Ответ: " + answer);
    }

    /**
     * Метод записывает строковый массив в файл
     *
     * @param answer - строковый массив
     */
    static void outputFile(ArrayList<String> answer) {
        try {
            FileWriter fileWriter = new FileWriter("src\\ru\\kra\\workingRepository\\calculator2\\textOutput");

            for (String s : answer) {
                if (!s.equals("")) {
                    fileWriter.write("Ответ: " + s + "\n");

                } else {
                    fileWriter.write("\n");
                }
            }

            fileWriter.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
