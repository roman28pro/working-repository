package ru.kra.workingRepository.main10;

public class Main {
    private static final String WAY = "src\\ru\\kra\\workingRepository\\main10\\oneTxt";
    private static final String WAY_2 = "src\\ru\\kra\\workingRepository\\main10\\oneCopy";
    private static final String WAY_3 = "src\\ru\\kra\\workingRepository\\main10\\twoTxt";
    private static final String WAY_4 = "src\\ru\\kra\\workingRepository\\main10\\twoCopy10";

    public static void main(String[] args) {
        long timeBefore = System.currentTimeMillis();
        CopyStream cs = new CopyStream(WAY, WAY_2);
        CopyStream cs2 = new CopyStream(WAY_3, WAY_4);
        cs.start();
        cs2.start();
        try {
            cs.join();
            cs2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        long timeAfter = System.currentTimeMillis();

        System.out.println("Время последовательного копирования = " + time(timeBefore, timeAfter) + " ms");

        timeBefore = System.currentTimeMillis();
        CopyFile cf = new CopyFile(WAY, WAY_2);
        cf.copy();
        CopyFile cf2 = new CopyFile(WAY_3, WAY_4);
        cf2.copy();
        timeAfter = System.currentTimeMillis();

        System.out.println("Время параллельного копирования = " + time(timeBefore, timeAfter) + " ms");
    }

    private static long time(long before, long after) {
        return after - before;
    }
}
