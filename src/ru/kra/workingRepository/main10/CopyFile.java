package ru.kra.workingRepository.main10;

import java.io.*;

class CopyFile {
    private String inputFileWay, outputFileWay;

    CopyFile(String inputFileWay, String outputFileWay) {
        this.inputFileWay = inputFileWay;
        this.outputFileWay = outputFileWay;
    }

    void copy() {
        try (BufferedReader br = new BufferedReader(new FileReader(inputFileWay))) {
            FileWriter fw = new FileWriter(outputFileWay);

            String str;
            while ((str = br.readLine()) != null) {
                fw.write(str + "\n");
            }

            fw.close();

        } catch (FileNotFoundException e) {
            System.out.println("Файл не найден!");
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}