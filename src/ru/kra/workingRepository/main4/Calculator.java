package ru.kra.workingRepository.main4;

/**
 * Класс MathInteger выполнят разные математические операции (Сложение, вычитание, умножение, деление и возведение в степень)
 *
 * @author Киреев Р.А. 17ИТ18
 */
public class Calculator {

    /**
     * Метод производит преобразование строки в целочисленное значение
     *
     * @param value - значение типа String
     * @return - возвращает преобразованное значение
     */
    public static int parseInt(String value) {
        return Integer.parseInt(value);
    }

    /**
     * @param str      - строка
     * @param valueOne
     * @param valueTwo
     */
    public static void methodCall(String str, int valueOne, int valueTwo) {
        switch (str) {
            case "*":
                System.out.println(multiply(valueOne, valueTwo));
                break;
            case "/":
                System.out.println(division(valueOne, valueTwo));
                break;
            case "+":
                System.out.println(addition(valueOne, valueTwo));
                break;
            case "-":
                System.out.println(subtraction(valueOne, valueTwo));
                break;
            case "^":
                System.out.println(pow(valueOne, valueTwo));
                break;
            default:
                System.out.println("Знака \"" + str + "\" нет в калькуляторе");
                break;
        }
    }

    public static int multiply(int numberOne, int numberTwo) {
        return numberOne * numberTwo;
    }

    public static int division(int numberOne, int numberTwo) {
        return numberOne / numberTwo;
    }

    public static int addition(int numberOne, int numberTwo) {
        return numberOne + numberTwo;
    }

    public static int subtraction(int numberOne, int numberTwo) {
        return numberOne - numberTwo;
    }

    public static int pow(int numberOne, int numberTwo) {
        if (numberOne < numberTwo) {
            int temp = numberOne;
            numberOne = numberTwo;
            numberTwo = temp;
        }

        int result = numberOne;

        for (int i = 0; i < numberTwo - 1; i++) {
            result *= numberOne;
        }
        return result;
    }
}