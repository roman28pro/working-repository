package ru.kra.workingRepository.main4;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите два числа и знак между ними через пробелы. Пример: *5 + 2*");
        try {
            String[] arrInputLine = scanner.nextLine().split(" ");

            int valueOne = Calculator.parseInt(arrInputLine[0]);
            int valueTwo = Calculator.parseInt(arrInputLine[2]);

            Calculator.methodCall(arrInputLine[1], valueOne, valueTwo);

        } catch (ArithmeticException e) {

            e.printStackTrace();

        } catch (NumberFormatException e) {
            System.out.println("Вы забыли ввести пробелы!");
            e.printStackTrace();
        }
    }
}