package ru.kra.workingRepository.main1;

@SuppressWarnings("unused")

public class Main {

    /**
     * Функция вычисления произведения
     *
     * @param a - первый множитель
     * @param b - второй множитель
     */

    public static int Calc(int a, int b) {
        int answer = 0;
        int multiplicand;
        int multiplier;
        if (a < b) {
            multiplicand = a;
            multiplier = b;
        } else {
            multiplicand = b;
            multiplier = a;
        }
        for (int i = 0; i < Math.abs(multiplier); i++) {
            if ((multiplicand < 0) && (multiplier < 0)) {
                answer -= multiplicand;
            } else {
                answer += multiplicand;
            }
        }
        return answer;
    }
}