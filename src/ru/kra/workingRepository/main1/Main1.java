package ru.kra.workingRepository.main1;

import java.util.Scanner;

/**
 * Класс содержит метод
 * для выполнения математической операции и ввода данных
 *
 * @author Roman Kireev 17IT18
 */

public class Main1 {
    public static void main(String[] args) {
        Main1 calc = new Main1();
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите число:");
        int a = sc.nextInt();
        System.out.println("Введите число на которое хотите умножить:");
        int b = sc.nextInt();
        System.out.println("Ответ: " + calc.Calc(a, b));
    }

    /**
     * Функция вычисления произведения
     *
     * @param a - первый множитель
     * @param b - второй множитель
     */

    private int Calc(int a, int b) {
        int answer = 0;
        int Multiplicand;
        int Multiplier;
        if (a < b) {
            Multiplicand = a;
            Multiplier = b;
        } else {
            Multiplicand = b;
            Multiplier = a;
        }
        for (int i = 0; i < Math.abs(Multiplier); i++) {
            if ((Multiplicand < 0) && (Multiplier < 0)) {
                answer -= Multiplicand;
            } else {
                answer += Multiplicand;
            }
        }
        return answer;
    }
}