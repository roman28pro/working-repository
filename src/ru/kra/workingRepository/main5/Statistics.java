package ru.kra.workingRepository.main5;

import java.io.*;
import java.util.Arrays;

public class Statistics {
    public static void main(String[] args) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("src\\ru\\kra\\workingRepository\\main5\\text"))) {
            String string;
            while ((string = bufferedReader.readLine()) != null) {
                String[] strings = string.split(" ");
                System.out.println(Arrays.toString(strings));
                Statistics wordCounter = new Statistics();
                System.out.println("Всего слов в строке: " + wordCounter.WordCounter(strings));
                String[] strings1 = string.split("");
                Statistics characterCounter = new Statistics();
                System.out.println("Всего символов в строке: " + characterCounter.CharacterCounter(strings1));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static int WordCounter(String[] strings) {
        int answer = 0;
        for (int i = 0; i <= strings.length; i++) {
            if (i != 0) {
                answer++;
            }
        }
        return answer;
    }

    private static int CharacterCounter(String[] strings1) {
        int answer = 0;
        for (int i = 0; i <= strings1.length; i++) {
            if (i != 0) {
                answer++;
            }
        }
        return answer;
    }
}
