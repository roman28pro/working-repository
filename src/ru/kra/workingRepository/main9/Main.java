package ru.kra.workingRepository.main9;

public class Main {
    public static void main(String[] args) {
        Dispute egg = new Dispute("egg");
        Dispute chicken = new Dispute("chicken");
        egg.start();
        chicken.start();

        try {
            chicken.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (egg.isAlive()) {
            System.out.println("Первым было яйцо");
        } else {
            System.out.println("Первой была курица");
        }
    }
}

class Dispute extends Thread {
    private String name;

    Dispute(String name) {
        this.name = name;
    }

    @Override
    public void run() {

        for (int i = 0; i < 10; i++) {

            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(name);
        }
    }
}