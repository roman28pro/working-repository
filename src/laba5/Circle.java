package laba5;

public class Circle {
    public static String typeDefinition(double k, double b, Circles circles) {
        double r = circles.getR();
        if (r < 0) {
            return "Радиус отрицателен";
        }

        try {
            double discriminant = getDiscriminant(0.0, k, b - Math.pow(r, 2));

            if (discriminant == 0) {
                return "Прямая касается окружности";
            }

            if (discriminant > 0) {
                return "Прямая проходит через окружность";
            }

            return "Нельзя опеределить";
        } catch (Exception e) {
            return "Окружность и прямая не пересекаются";
        }
    }

    private static double getDiscriminant(double a, double b, double c) {
        return b * b - 4 * a * c;
    }
}
