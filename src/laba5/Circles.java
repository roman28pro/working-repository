package laba5;

public class Circles {

    private final double x;
    private final double y;
    private final double r;

    public Circles(double x, double y, double r) {
        this.x = x;
        this.y = y;
        this.r = r;
    }

    public Circles(double r) {
        this.r = r;
        this.x = 0;
        this.y = 0;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getR() {
        return r;
    }

    public static String checkCircleOnType(Circles firstCircle, Circles secondCircle) {
        double r1 = firstCircle.getR();
        double r2 = secondCircle.getR();

        if ((r1 < 0) || (r2 < 0)) {
            return "Радиус отрицателен";
        }

        if (r1 < r2) {
            double swap = r1;
            r1 = r2;
            r2 = swap;
        }

        if (firstCircle.getX() == secondCircle.getX() && firstCircle.getY() == secondCircle.getY() && firstCircle.getR() == secondCircle.getR()) {
            return "Окружности равны";
        }

        if (firstCircle.getX() == secondCircle.getX() && firstCircle.getY() == secondCircle.getY()) {
            return "Окружности коллениальны";
        }

        double d = Math.sqrt(Math.pow((firstCircle.getX() - secondCircle.getX()), 2) + Math.pow((firstCircle.getY() - secondCircle.getY()), 2));

        if (d > firstCircle.getR() + secondCircle.getR()) {
            return "Окружности не пересекаются";
        }

        if (d == firstCircle.getR() - secondCircle.getR()) {
            return "Окружности пересекаются в одной точке";
        }

        if ((firstCircle.getR() - secondCircle.getR() > d) && (d < firstCircle.getR() + secondCircle.getR())) {
            return "Окружности пересекаются в двух точках";
        }

        return "Невозможно определить";
    }
}
