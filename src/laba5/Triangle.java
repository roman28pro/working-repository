package laba5;


public class Triangle {
    private final double a;
    private final double b;
    private final double c;

    public Triangle(double a, double b, double c){
        this.a=a;
        this.b=b;
        this.c=c;
    }

    public String whatIsTheTriangle(){
        if ((a+b+c!=180)||(a<0||b<0||c<0)){
            return "Некорректное значение треугольника";
        }

        if (a==90||b==90||c==90){
            return "Прямоугольный";
        }else if((90<a&&a<180)||(90<b&&b<180)||(90<c&&c<180)){
            return "Тупоугольный";
        }else if(a==60&&b==60&&c==60){
            return "Равносторонний";
        }else if(a==b||a==c||b==c){
            return "Равнобедренный";
        }else if(a<90&&b<90&&c<90){
            return "Остроугольный";
        }else {
            return "Не удалось определить";
        }
    }
}
