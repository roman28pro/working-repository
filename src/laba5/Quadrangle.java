package laba5;

import java.awt.*;

public class Quadrangle {

    private final Point a;
    private final Point b;
    private final Point c;
    private final Point d;

    public Quadrangle(Point a, Point b, Point c, Point d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }

    public boolean isSquare() {
        return !isRhombus();
    }

    public boolean isRhombus() {
        double dp1 = differentPoints(a, b);
        double dp2 = differentPoints(b, c);
        double dp3 = differentPoints(c, d);
        double dp4 = differentPoints(d, a);
        return dp1 * dp2 == -1 && dp2 * dp3 == -1 && dp3 * dp4 == -1 && dp4 * dp1 == -1;
    }
    private double differentPoints(Point a, Point b){
        return (a.getY()-b.getY())/(a.getX()-b.getX());
    }
}
