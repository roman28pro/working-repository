package laba5;

import java.awt.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Quadrangle quadrangle;
        Circles firstCircle, secondCircle;
        System.out.println("Введите вариант от 1 до 5:");
        switch (sc.nextLine()) {
            case "1":
                try {
                    System.out.println("Введите углы треугольника: ");
                    Triangle triangle = new Triangle(sc.nextDouble(), sc.nextDouble(), sc.nextDouble());
                    System.out.println(triangle.whatIsTheTriangle());
                }catch (Exception e){
                    System.out.println("Некорректные данные");
                }
                break;
            case "2":
                try {
                System.out.println("Введите координаты четырехугольника: ");
                quadrangle = new Quadrangle(
                        new Point(sc.nextInt(), sc.nextInt()),
                        new Point(sc.nextInt(), sc.nextInt()),
                        new Point(sc.nextInt(), sc.nextInt()),
                        new Point(sc.nextInt(), sc.nextInt())
                );

                if (quadrangle.isSquare()) {
                    System.out.println("Четырехугольник является квадратом");
                } else {
                    System.out.println("Четырехугольник не является квадратом");
                }
        }catch (Exception e){
            System.out.println("Некорректные данные");
        }
                break;
            case "3":
                try {
                System.out.println("Введите координаты предполагаемого ромба");
                quadrangle = new Quadrangle(
                        new Point(sc.nextInt(), sc.nextInt()),
                        new Point(sc.nextInt(), sc.nextInt()),
                        new Point(sc.nextInt(), sc.nextInt()),
                        new Point(sc.nextInt(), sc.nextInt())
                );

                if (quadrangle.isRhombus()) {
                    System.out.println("Четырехугольник является ромбом");
                } else {
                    System.out.println("Четырехугольник не является ромбом");
                }
    }catch (Exception e){
        System.out.println("Некорректные данные");
    }
                break;
            case "4":
                try {
                System.out.println("Введите координаты центра (Х и Y) и радиус(R)");
                Circles circles = new Circles(sc.nextDouble());
                System.out.println(Circle.typeDefinition(sc.nextDouble(), sc.nextDouble(), circles));
}catch (Exception e){
        System.out.println("Некорректные данные");
        }
                break;
            case "5":
                try {
                System.out.println("Введите координаты центра (Х и Y) и радиус(R)");
                firstCircle = new Circles(sc.nextDouble(), sc.nextDouble(), sc.nextDouble());
                secondCircle = new Circles(sc.nextDouble(), sc.nextDouble(), sc.nextDouble());
                Circles.checkCircleOnType(firstCircle, secondCircle);
                System.out.println(Circles.checkCircleOnType(firstCircle,secondCircle));
        }catch (Exception e){
        System.out.println("Некорректные данные");
        }
                break;
            default:
                System.out.println("Некорректный формат");
        }
    }
}

