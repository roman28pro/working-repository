/*
package practice2;

import com.mysql.cj.jdbc.exceptions.CommunicationsException;
import javafx.collections.FXCollections;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.MapValueFactory;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

public class DatabaseEditorController {


    @FXML
    private TableView<HashMap<String, String» tableView;

    @FXML
    private Button addRecord;

    @FXML
    private Button changeRecord;

    @FXML
    private Button removeRecord;

    @FXML
    private Button exitBtn;

    @FXML
    private Button payrollBtn;

    @FXML
    private ListView<String> listTables;

    MultipleSelectionModel<String> listTablesSelectionModel;

    private Database database;

    private EventHandler<WindowEvent> crossClosing = new EventHandler<WindowEvent>() {
        @Override
        public void handle(WindowEvent event) {
            try {
                database.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    };

    private Stage stage;

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public Database getDatabase() {
        return database;
    }

    public String getCurrentTable() {
        return listTablesSelectionModel.getSelectedItem();
    }


    public EventHandler<WindowEvent> getCrossClosing() {
        return crossClosing;
    }

    @FXML
    public void initialize() {
        try {
            database = connectDatabase("jdbc:mysql://localhost/информация о студентах?serverTimezone=Europe/Moscow&useSSL=false", "root", null);

            ArrayList<String> tableNames = database.getTableNames();
            listTables.setItems(FXCollections.observableArrayList(tableNames));

            handle();

        } catch (CommunicationsException e) {
            infoWindow(Alert.AlertType.ERROR, "Ошибка подключения", null, "Невозможно подключиться к базе данных!\nПроверьте соединение!");
            e.printStackTrace();
            System.exit(e.getErrorCode());

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private Database connectDatabase(String url, String user, String password) throws SQLException {
        Database database = new Database(url, user, password);
        database.connect();
        return database;
    }

    private void infoWindow(Alert.AlertType type, String title, String headerText, String contentText) {
        Alert alert = new Alert(type);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.setContentText(contentText);

        alert.showAndWait();
    }

    public void close() {
        stage.close();
    }

    private void handle() {
        exitBtn.setOnAction(event -> {
            try {
                database.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            close();
        });

        listTablesSelectionModel = listTables.getSelectionModel();
        listTablesSelectionModel.selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            try {
                showTableData(newValue);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });

        removeRecord.setOnAction(event -> {
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/removeRecord.fxml"));
                Parent root = fxmlLoader.load();
                Stage stage = new Stage();
                stage.setTitle("Удаление записи");
                stage.centerOnScreen();
                stage.setScene(new Scene(root));
                stage.show();

                RemoveRecordController removeRecordController = fxmlLoader.getController();
                removeRecordController.setDatabaseEditorController(this);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        addRecord.setOnAction(event -> {
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/addRecord.fxml"));
                AddRecordController addRecordController = new AddRecordController(this);
                fxmlLoader.setController(addRecordController);
                Parent root = fxmlLoader.load();
                Stage stage = new Stage();
                stage.setTitle("Добавление записи");
                stage.centerOnScreen();
                stage.setScene(new

                        Scene(root));
                stage.show();

            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        changeRecord.setOnAction(event -> {
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/changeRecord.fxml"));
                ChangeRecordController changeRecordController = new ChangeRecordController(this);
                fxmlLoader.setController(changeRecordController);
                Parent root = fxmlLoader.load();
                Stage stage = new Stage();
                stage.setTitle("Изменение записи");
                stage.centerOnScreen();
                stage.setScene(new Scene(root));
                stage.show();

            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        payrollBtn.setOnAction(event -> {
            try {
                tableView.getColumns().clear();
                String tableName = "сведения о студентах";
                int numberColumns = database.countColumns(tableName);
                ArrayList<String> columnNames = database.getColumnNames(tableName, numberColumns, 3);
                ArrayList<TableColumn<HashMap<String, String>, String» columns = createColumns(columnNames);
                tableView.setItems(FXCollections.observableArrayList(getItems(tableName, columnNames)));
                setValuesColumns(columns, columnNames);
                tableView.getColumns().addAll(columns);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    public void showTableData(String tableName) throws SQLException {
        tableView.getColumns().clear();

        int numberColumns = database.countColumns(tableName);

        ArrayList<String> columnNames = database.getColumnNames(tableName, numberColumns, 1);

        ArrayList<TableColumn<HashMap<String, String>, String» columns = createColumns(columnNames);

        tableView.setItems(FXCollections.observableArrayList(getItems(tableName, columnNames)));

        setValuesColumns(columns, columnNames);

        tableView.getColumns().addAll(columns);
    }

    private ArrayList<TableColumn<HashMap<String, String>, String» createColumns(ArrayList<String> columnNames) {
        ArrayList<TableColumn<HashMap<String, String>, String» columns = new ArrayList<>();

        for (String columnName : columnNames) {
            TableColumn<HashMap<String, String>, String> tableColumn = new TableColumn<>(columnName);
            columns.add(tableColumn);
        }

        return columns;
    }

    private ArrayList<HashMap<String, String» getItems(String tableName, ArrayList<String> columnNames) throws SQLException {
        ArrayList<HashMap<String, String» items = new ArrayList<>();
        ResultSet rs = database.request("SELECT * FROM `" + tableName + "`");
        while (rs.next()) {
            HashMap<String, String> value = new HashMap<>();
            for (int i = 0; i < columnNames.size(); i++) {
                value.put(columnNames.get(i), rs.getString(columnNames.get(i)));
            }
            items.add(value);
        }
        rs.close();

        return items;
    }

    private void setValuesColumns(ArrayList<TableColumn<HashMap<String, String>, String» columns, ArrayList<String> columnNames) {
        for (int i = 0; i < columnNames.size(); i++) {
            columns.get(i).setCellValueFactory(new MapValueFactory(columnNames.get(i)));
        }
    }
}
 */