package practice2;

import java.sql.*;
import java.util.ArrayList;

public class Database {
    private String url;
    private String user;
    private String password;

    private Connection con;
    private Statement stmt;

    public Database(String url, String user, String password) {
        this.url = url;
        this.user = user;
        this.password = password;
    }

    public void connect() throws SQLException {
        con = DriverManager.getConnection(url, user, password);
        stmt = con.createStatement();
    }

    public void close() throws SQLException {
        con.close();
        stmt.close();
    }

    public ResultSet request(String sql) throws SQLException {
        return stmt.executeQuery(sql);
    }

    public void requestUpdate(String sql) throws SQLException {
        stmt.executeUpdate(sql);
    }

    public int countColumns(String tableName) throws SQLException {
        return request("SELECT * FROM `" + tableName + "`;").getMetaData().getColumnCount();
    }

    public ArrayList<String> getColumnNames(String tableName, int numberColumns, int startColumn) throws SQLException {
        ArrayList<String> columnNames = new ArrayList<>();

        ResultSetMetaData rsm = request("SELECT * FROM `" + tableName + "`;").getMetaData();
        for (int i = startColumn; i <= numberColumns; i++) {
            columnNames.add(rsm.getColumnName(i));
        }

        return columnNames;
    }

    public ArrayList<String> getTableNames() throws SQLException {
        ArrayList<String> tableNames = new ArrayList<>();

        ResultSet rs = request("SHOW TABLES;");
        while (rs.next()) {
            tableNames.add(rs.getString(1));
        }
        rs.close();

        return tableNames;
    }
}