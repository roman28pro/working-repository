package demo.KRA.arithmetic;

import org.junit.Test;

import static org.junit.Assert.*;

public class ArithmeticTest {

    @Test
    public void multirlyPositive() {
        int a = 1 + (int) (Math.random() * 200);
        int b = 1 + (int) (Math.random() * 100);
        assertEquals(a * b, Arithmetic.multirly(a, b));
    }

    @Test
    public void multirlyZeroByZero() {
        int a = 0;
        int b = 0;
        assertEquals(a * b, Arithmetic.multirly(a, b));
    }
}