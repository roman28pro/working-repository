package laba3;

import org.junit.Assert;
import org.junit.Test;

public class MainTest {
    @Test
    public void calculatedPositive(){
        Assert.assertEquals(3,Main.calculated(3,2),0);
    }
    @Test
    public void calculatedNegative(){
        Assert.assertEquals(15,Main.calculated(3,-2),0);
    }
    @Test
    public void calculatedAllNegative(){
        Assert.assertEquals(15,Main.calculated(-3,-2),0);
    }
    @Test
    public void calculatedZero(){
        Assert.assertEquals(0,Main.calculated(0,0),0);
    }
}