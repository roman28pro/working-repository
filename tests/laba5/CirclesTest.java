package laba5;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class CirclesTest {

    @Test
    public void collegiality(){
        Circles circles=new Circles(1,1,1);
        Circles circles1=new Circles(1,1,2);
        String extepted="Окружности коллениальны";
        String actual=Circles.checkCircleOnType(circles,circles1);
        Assert.assertEquals(extepted,actual);
    }

    @Test
    public void onePoint(){
        Circles circles=new Circles(1,5,5);
        Circles circles1=new Circles(1,1,1);
        String extepted="Окружности пересекаются в одной точке";
        String actual=Circles.checkCircleOnType(circles,circles1);
        Assert.assertEquals(extepted,actual);
    }

}