package laba5;

import org.junit.Test;

import static org.junit.Assert.*;

public class TriangleTest {

    @Test
    public void inncorrect(){
        Triangle triangle = new Triangle(231.0,31.0,32.0);
        String actual=triangle.whatIsTheTriangle();
        String excepted="Некорректное значение треугольника";
        assertEquals(excepted,actual);
    }

    @Test
    public void inncorrect2(){
        Triangle triangle = new Triangle(1.0,2.0,3.0);
        String actual=triangle.whatIsTheTriangle();
        String excepted="Некорректное значение треугольника";
        assertEquals(excepted,actual);
    }

    @Test
    public void inncorrect3(){
        Triangle triangle = new Triangle(100.0,-5.0,85.0);
        String actual=triangle.whatIsTheTriangle();
        String excepted="Некорректное значение треугольника";
        assertEquals(excepted,actual);
    }

    @Test
    public void obtuse(){
        Triangle triangle = new Triangle(100.0,20.0,60.0);
        String actual=triangle.whatIsTheTriangle();
        String excepted="Тупоугольный";
        assertEquals(excepted,actual);
    }

    @Test
    public void rectangular(){
        Triangle triangle = new Triangle(90.0,45.0,45.0);
        String actual=triangle.whatIsTheTriangle();
        String excepted="Прямоугольный";
        assertEquals(excepted,actual);
    }

    @Test
    public void acute(){
        Triangle triangle = new Triangle(70.0,80.0,30.0);
        String actual=triangle.whatIsTheTriangle();
        String excepted="Остроугольный";
        assertEquals(excepted,actual);
    }

    @Test
    public void isosceles(){
        Triangle triangle = new Triangle(70.0,70.0,40.0);
        String actual=triangle.whatIsTheTriangle();
        String excepted="Равнобедренный";
        assertEquals(excepted,actual);
    }

    @Test
    public void equilateral(){
        Triangle triangle = new Triangle(60.0,60.0,60.0);
        String actual=triangle.whatIsTheTriangle();
        String excepted="Равносторонний";
        assertEquals(excepted,actual);
    }

}