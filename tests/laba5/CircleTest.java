package laba5;

import org.junit.Test;

import static org.junit.Assert.*;

public class CircleTest {

    @Test
public void incorrect(){
        Circles circles=new Circles(-5);
        String extepted="Радиус отрицателен";
        String actual=Circle.typeDefinition(0,0,circles);
        assertEquals(extepted,actual);
    }

    @Test
    public void touches(){
        Circles circles=new Circles(30);
        String extepted="Прямая касается окружности";
        String actual=Circle.typeDefinition(0,1,circles);
        assertEquals(extepted,actual);
    }

    @Test
    public void isPassing(){
        Circles circles=new Circles(5);
        String extepted="Прямая проходит через окружность";
        String actual=Circle.typeDefinition(2,3,circles);
        assertEquals(extepted,actual);
    }

    @Test
    public void onePoint(){
        Circles circles=new Circles(5);
        String extepted="Прямая проходит через окружность";
        String actual=Circle.typeDefinition(2,3,circles);
        assertEquals(extepted,actual);
    }

    @Test
    public void twoPoint(){
        Circles circles=new Circles(5);
        String extepted="Прямая проходит через окружность";
        String actual=Circle.typeDefinition(2,3,circles);
        assertEquals(extepted,actual);
    }
}