package laba5;

import org.junit.Test;

import java.awt.*;

import static org.junit.Assert.*;

public class QuadrangleTest {

    @Test
    public void square(){
        Quadrangle quadrangle=new Quadrangle(
                new Point(2,1),
                new Point(3,2),
                new Point(2,3),
                new Point(1,2));
        assertTrue(quadrangle.isRhombus());
    }

    @Test
    public void rhombus(){
        Quadrangle quadrangle=new Quadrangle(
                new Point(1,1),
                new Point(3,1),
                new Point(3,3),
                new Point(1,3));
        assertTrue(quadrangle.isSquare());
    }

    @Test
    public void otherwise(){
        Quadrangle quadrangle=new Quadrangle(
                new Point(1,1),
                new Point(3,1),
                new Point(3,3),
                new Point(1,3));
        assertFalse(quadrangle.isRhombus());
    }

}