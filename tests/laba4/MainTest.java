package laba4;

import org.junit.Assert;
import org.junit.Test;

import static java.lang.Float.NaN;

public class MainTest {

    @Test
    public void positiveValue(){
        Assert.assertEquals(-54.29409129247696, Main.answer(30),0);
    }

    @Test
    public void negativeValue(){
        Assert.assertEquals(2296.194186065593, Main.answer(-30),0);
    }

    @Test
    public void zeroValue(){
        Assert.assertEquals(NaN, Main.answer(0),0);
    }
}