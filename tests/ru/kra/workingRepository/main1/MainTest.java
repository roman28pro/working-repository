package ru.kra.workingRepository.main1;

import org.junit.Test;

import static org.junit.Assert.*;

public class MainTest {

    @Test
    public void mainPositive() {
        int a = 1 + (int) (Math.random() * 200);
        int b = 1 + (int) (Math.random() * 100);
        assertEquals(a * b, Main.Calc(a, b));
    }
}